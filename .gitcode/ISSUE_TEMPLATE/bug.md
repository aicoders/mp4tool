---
name: "Bug 报告"
about: "报告一个问题帮助我们改进"
title: "【BUG】:"
labels: ["BUG"]
assignees: 'username'
---

### BUG 类型

<!-- 
请在这里描述你所遇到的 BUG 类型，以便我们更快定位问题，比如 UI、功能、体验等 
-->

### 复现步骤

<!-- 
请在这里描述你遇到该 BUG 时的页面及步骤
-->

