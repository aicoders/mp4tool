import subprocess

def run_ffmpeg_command(command):
    """运行FFmpeg命令"""
    try:
        subprocess.run(command, check=True, shell=True)
    except subprocess.CalledProcessError as e:
        print(f"Error: {e}")

def convert_video_format(input_file, output_file, output_format):
    """转换视频格式"""
    command = f"ffmpeg -i {input_file} {output_file}.{output_format}"
    run_ffmpeg_command(command)

def extract_audio(input_file, output_file):
    """提取音频"""
    command = f"ffmpeg -i {input_file} -q:a 0 -map a {output_file}.mp3"
    run_ffmpeg_command(command)

def create_video_thumbnail(input_file, output_image, time="00:00:01"):
    """创建视频缩略图"""
    command = f"ffmpeg -i {input_file} -ss {time} -vframes 1 {output_image}.png"
    run_ffmpeg_command(command)

def resize_video(input_file, output_file, width, height):
    """调整视频大小"""
    command = f"ffmpeg -i {input_file} -vf scale={width}:{height} {output_file}"
    run_ffmpeg_command(command)
# sanp a picture from a video file
def sanp_pic(input_file, output_image, time="00:00:01"):
    """创建视频缩略图"""
    command = f"ffmpeg -i {input_file} -ss {time} -vframes 1 {output_image}.png"
    run_ffmpeg_command(command)
# 
def detect_sub(input_file):
    """检测字幕信息"""
    command = f"ffmpeg -i {input_file} "
    run_ffmpeg_command(command)

# 示例用法
if __name__ == "__main__":
    convert_video_format("input.mp4", "output", "avi")
    extract_audio("input.mp4", "audio_output")
    create_video_thumbnail("input.mp4", "thumbnail")
    resize_video("input.mp4", "resized_output.mp4", 640, 480)

